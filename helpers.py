import os
import sys
import math
from qgis.core import (QgsVectorLayer, QgsPointXY, QgsField, QgsWkbTypes,
                       QgsDistanceArea, QgsCoordinateTransformContext, QgsUnitTypes,
                       QgsFeature, QgsGeometry, QgsVectorFileWriter)
from PyQt5.QtCore import QVariant


class Line:
    def __init__(self, points, chainages, angles):
        self.pointsXY = points
        self.chainages = chainages
        self.angles = angles


class Point:
    def __init__(self, point, z):
        self.pointXY = point
        self.z = z


def interpolate(a, b, c, d, e):
    """
    Linear interpolation

    :param a: known mid point
    :param b: known lower value
    :param c: known upper value
    :param d: unknown lower value
    :param e: unknown upper value
    :return: float
    """

    a = float(a)
    b = float(b)
    c = float(c)
    d = float(d)
    e = float(e)

    return (e - d) / (c - b) * (a - b) + d


def is1dTable(layer):
    """
    Checks if a layer is a 1d_ta type

    :param layer: QgsVectorLayer
    :return: bool
    """

    if not isinstance(layer, QgsVectorLayer):
        return False

    correct1dTableType = [QVariant.String, QVariant.String, QVariant.String, QVariant.String, QVariant.String,
                          QVariant.String, QVariant.String, QVariant.String, QVariant.String]

    fieldTypes = []
    for i, f in enumerate(layer.getFeatures()):
        if i > 0:
            break
        fields = f.fields()
        if fields.count() < 9:
            return False
        for j in range(0, 9):
            field = fields.field(j)
            fieldType = field.type()
            fieldTypes.append(fieldType)

    if fieldTypes == correct1dTableType:
        return True
    else:
        return False


def create_empty_2d_zsh(fname, crs, geom):
    """
    Creates empty 2d_zsh empty type

    :param fname: str
    :param crs: QgsCoordinateReferenceSystem
    :param geom: str
    :return: QgsVectorLayer
    """

    if geom.lower() == 'p':
        uri = "point?crs={0}".format(crs.authid().lower())
    elif geom.lower() == 'l':
        uri = "linestring?crs={0}".format(crs.authid().lower())
    else:
        print("ERROR: unable to initialise {1} - unrecognised geometry type: {0}".format(geom, os.path.basename(fname)))
        return
    lyr = QgsVectorLayer(uri, os.path.basename(fname), "memory")
    dp = lyr.dataProvider()
    attributes = [
        QgsField('Z', QVariant.Double, len=23, prec=15),
        QgsField('dZ', QVariant.Double, len=23, prec=15),
        QgsField('Shape_Widt', QVariant.Double, len=23, prec=15),
        QgsField('Shape_Opti', QVariant.String, len=20),
        QgsField('Comment', QVariant.String, len=20)
    ]
    dp.addAttributes(attributes)
    lyr.updateFields()

    return lyr


def calculateLength(p1, p2, crs=None):
    """

    """

    da = QgsDistanceArea()
    da.setSourceCrs(crs, QgsCoordinateTransformContext())
    return da.convertLengthMeasurement(da.measureLine(p1, p2), QgsUnitTypes.DistanceMeters)


def calculate_angle(p1, p2):
    """

    """

    return math.atan2((p2.y() - p1.y()), (p2.x() - p1.x()))


def line_geometry(feat, id=""):
    if feat.geometry().wkbType() == QgsWkbTypes.LineString or \
            feat.geometry().wkbType() == QgsWkbTypes.LineStringZ or \
            feat.geometry().wkbType() == QgsWkbTypes.LineStringM or \
            feat.geometry().wkbType() == QgsWkbTypes.LineStringZM:
        geom = feat.geometry().asPolyline()
    elif feat.geometry().wkbType() == QgsWkbTypes.MultiLineString or \
            feat.geometry().wkbType() == QgsWkbTypes.MultiLineStringZ or \
            feat.geometry().wkbType() == QgsWkbTypes.MultiLineStringM or \
            feat.geometry().wkbType() == QgsWkbTypes.MultiLineStringZM:
        mGeom = feat.geometry().asMultiPolyline()
        geom = []
        for g in mGeom:
            for p in g:
                geom.append(p)
    elif feat.geometry().wkbType() == QgsWkbTypes.Unknown:
        print("ERROR: feature {0} has an unknown geometry... skipping".format(id))
        return None
    else:
        print("ERROR: feature {0} has an unknown geometry... skipping".format(id))
        return None

    return geom


def line_to_points(feat, id, crs):
    points = line_geometry(feat, id)
    if points is None:
        return
    chainages = [0.] + [calculateLength(points[x-1], points[x], crs) for x in range(1, len(points))]
    chainages = [sum(chainages[:x+1]) for x in range(len(chainages))]
    angles = [calculate_angle(points[x], points[x+1]) for x in range(len(points) - 1)]
    return Line(points, chainages, angles)


def get_x(origin, angle, dist):
    cosang = math.cos(angle)
    dx = dist * cosang
    return origin + dx


def get_y(origin, angle, dist):
    sinang = math.sin(angle)
    dy = dist * sinang
    return origin + dy


def pointXY(ch, line):
    pointXY = None
    ang = line.angles[0]
    replace_point = False
    for i, c in enumerate(line.chainages):
        if c == ch:
            pointXY = line.pointsXY[i]
            replace_point = True
            break
        if c > ch:
            i -= 1
            if i >= 0:
                ang = line.angles[i]
            i += 1
            break
        if i + 1 == len(line.chainages):
            ang = line.angles[i-1]
    x = get_x(line.pointsXY[i].x(), ang, ch - line.chainages[i])
    y = get_y(line.pointsXY[i].y(), ang, ch - line.chainages[i])
    return QgsPointXY(x, y), i, replace_point


def addPointsToZsh(points, lyr1, lyr2, id):
    # lyr1 is points layer
    features = []
    for p in points:
        if p.z == -99999:
            continue
        feat = QgsFeature()
        feat.setGeometry(QgsGeometry.fromPointXY(p.pointXY))
        attributes = [p.z, 0., 0., "", id]
        feat.setAttributes(attributes)
        features.append(feat)
    dp1 = lyr1.dataProvider()
    dp1.addFeatures(features)
    lyr1.updateExtents()

    # lyr2 is line layer
    if len([x.z for x in points if x.z != -99999]) >= 2:
        while points and points[-1].z == -99999:
            points.pop()
        while points and points[0].z == -99999:
            points.pop(0)
    if points:
        feat = QgsFeature()
        feat.setGeometry(QgsGeometry.fromPolylineXY([x.pointXY for x in points]))
        attributes = [0., 0., 0., "tin", id]
        feat.setAttributes(attributes)
        dp2 = lyr2.dataProvider()
        dp2.addFeature(feat)
        lyr2.updateExtents()


def writeLayer(layer, fpath, drivername):
    save_options = QgsVectorFileWriter.SaveVectorOptions()
    save_options.actionOnExistingFile = QgsVectorFileWriter.CreateOrOverwriteFile
    save_options.driverName = drivername
    QgsVectorFileWriter.writeAsVectorFormatV2(layer, fpath, QgsCoordinateTransformContext(), save_options)


