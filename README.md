# ESTRY_to_ZSH

Python script to convert ESTRY cross sections inputs into 2d_zsh line and points. The tool converts XZ cross section
data from CSV files into a XYZ GIS point layer (in a 2d_zsh format). The tool requires the TUFLOW 1d_xs layer and 
corresponding CSV files.

## Dependencies

The script in intended to be run in QGIS 3.10+ and all dependencies should be installed with the
QGIS Python environment.

## Quickstart

The Python script has been set up to run in QGIS 3.10+.

1. Open QGIS and open the 1d_xs GIS layer that is going to be converted
   * note: the 1d_xs needs to be valid i.e. the source attribute field should be a valid relative reference 
   to a CSV file containing the XZ data
2. Open the Python Console in QGIS
3. Open the Python editor in the console ('Show Editor' button in the Python Console toolbar)
4. Use 'Open Script' in the toolbar and open `estry_to_zsh.py`
5. Update `SCRIPT_LOCATION` variable to point to where `estry_to_zsh.py`, `estrydb_data_provider.py`, 
   and `helpers.py` are located (they should all be kept in the same folder)
6. Update `OUTPUT_LOCATION` variable to point to the desired directory where output GIS files will be
   written to
7. Select the 1d_xs layer in the 'Layers Panel' in QGIS
8. Run `estry_to_python.py` by pressing 'Run Script' in the Python editor

## More Details

Please see the project wiki page for more explanation and pretty pictures.

## Changelog

0.1
* first commit